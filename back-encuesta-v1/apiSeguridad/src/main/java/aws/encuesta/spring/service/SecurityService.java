package aws.encuesta.spring.service;

import aws.encuesta.spring.dto.RenewPasswordFirstDTO;
import aws.encuesta.spring.dto.RespuestaApi;
import aws.encuesta.spring.dto.UpdatePasswordDTO;

public interface SecurityService {

	public RespuestaApi getToken(String username, String password);
	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword);
	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword);
	public RespuestaApi signOut(String token);
	public RespuestaApi refreshToken(String token);
}
