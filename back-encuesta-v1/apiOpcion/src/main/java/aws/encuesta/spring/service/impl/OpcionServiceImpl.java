package aws.encuesta.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aws.encuesta.spring.dao.IOpcionDao;
import aws.encuesta.spring.model.Opcion;
import aws.encuesta.spring.service.IOpcionService;

@Service
public class OpcionServiceImpl implements IOpcionService {

	@Autowired
	private IOpcionDao opcionDao;
	
	@Override
	public List<Opcion> obtenerOpciones() {
		return opcionDao.findAll();
	}

}
