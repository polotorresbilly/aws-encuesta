package aws.encuesta.spring.service;

import java.util.List;

import aws.encuesta.spring.model.Opcion;

public interface IOpcionService {
	List<Opcion> obtenerOpciones();
}
