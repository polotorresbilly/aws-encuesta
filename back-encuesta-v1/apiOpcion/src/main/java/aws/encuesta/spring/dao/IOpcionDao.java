package aws.encuesta.spring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import aws.encuesta.spring.model.Opcion;

@Repository
public interface IOpcionDao extends JpaRepository<Opcion, Integer>{

}
