package aws.encuesta.spring.controller.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aws.encuesta.spring.model.Opcion;
import aws.encuesta.spring.service.IOpcionService;

@RestController
@CrossOrigin
@RequestMapping("api/opcion")
public class ApiOpcion {
	private static final Logger logger = LoggerFactory.getLogger(ApiOpcion.class);
	
	@Autowired
	private IOpcionService serviceOpcion;
	
	@GetMapping(value="listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Opcion>> obtenerTodos(){
		try {
			return new ResponseEntity<List<Opcion>>(
					serviceOpcion.obtenerOpciones(), HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
