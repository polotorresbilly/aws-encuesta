package aws.encuesta.spring.controller.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aws.encuesta.spring.dto.RespuestaApi;
import aws.encuesta.spring.model.Votacion;
import aws.encuesta.spring.service.IVotacionService;

@RestController
@CrossOrigin
@RequestMapping("api/votacion")
public class ApiVotacion {
	
	private static final Logger logger = LoggerFactory.getLogger(ApiVotacion.class);

	@Autowired
	private IVotacionService serviceVotacion;
	
	@PreAuthorize("hasRole('ROLE_ADMINISTRADORES')")
	@GetMapping(value="listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Votacion>> obtenerTodos(){
		try {
			return new ResponseEntity<List<Votacion>>(
					serviceVotacion.listarVotacion(), HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value="obtenerVotacionUsuario", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Votacion> obtenerVotacion(@RequestBody Votacion votacion){
		try {
			return new ResponseEntity<Votacion>(
					serviceVotacion.obtenerVotacionByUsuario(votacion), HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value="registrar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> guardar(
			@RequestBody Votacion Votacion){
		try {
			serviceVotacion.guardarVotacion(Votacion);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMINISTRADORES')")
	@GetMapping(value="listarpaginado", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> obtenerTodosPaginados(Pageable pageable){
		try {
			return new ResponseEntity<Page<Votacion>>(
					serviceVotacion.obtenerDatosPaginados(pageable), HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMINISTRADORES')")
	@DeleteMapping(value="eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> eliminar(
			@PathVariable int id){
		try {
			Votacion votacion = new Votacion();
			votacion.setId(id);
			serviceVotacion.eliminarVotacion(votacion);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
