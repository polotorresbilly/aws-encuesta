package aws.encuesta.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import aws.encuesta.spring.dao.IVotacionDao;
import aws.encuesta.spring.model.Votacion;
import aws.encuesta.spring.service.IVotacionService;

@Service
public class VotacionServiceImpl implements IVotacionService {

	@Autowired
	private IVotacionDao votacionDao;
	
	@Override
	public void guardarVotacion(Votacion votacion) {
		votacionDao.save(votacion);
	}

	@Override
	public List<Votacion> listarVotacion() {
		return votacionDao.findAll();
	}

	@Override
	public Votacion obtenerVotacionByUsuario(Votacion votacion) {
		return votacionDao.obtenerVotacionxUsuario(votacion.getUsuario());
	}
	
	@Override
	public Page<Votacion> obtenerDatosPaginados(Pageable pageable) {
		return this.votacionDao.findAll(pageable);
	}

	@Override
	public void eliminarVotacion(Votacion votacion) {
		votacionDao.delete(votacion);
	}


}
