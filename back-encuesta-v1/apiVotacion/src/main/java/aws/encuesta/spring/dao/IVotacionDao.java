package aws.encuesta.spring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import aws.encuesta.spring.model.Votacion;

@Repository
public interface IVotacionDao extends JpaRepository<Votacion, Integer>{
	@Query("SELECT t FROM Votacion t WHERE t.usuario = ?1")
	Votacion obtenerVotacionxUsuario(String usuario);
}
