package aws.encuesta.spring.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import aws.encuesta.spring.model.Votacion;

public interface IVotacionService {
	void guardarVotacion(Votacion votacion);
	List<Votacion> listarVotacion();
	Votacion obtenerVotacionByUsuario(Votacion votacion);
	Page<Votacion> obtenerDatosPaginados(Pageable pageable);
	void eliminarVotacion(Votacion votacion);
}
