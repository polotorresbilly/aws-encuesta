import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './material/material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SecurityComponent } from './pages/security/security.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { TokenInterceptorService } from './_services/token-interceptor.service';
import { AdminComponent } from './pages/admin/admin/admin.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { BodyComponent } from './pages/body/body.component';
import { LoginComponent } from './pages/login/login.component';
import { ErrorComponent } from './pages/login/error/error.component';
import { EncuestaComponent } from './pages/encuesta/encuesta.component';
import { AdminEncuestaComponent } from './pages/admin/encuesta/adminencuesta.component';
import { ActualizarEncuestaComponent } from './pages/admin/encuesta/actualizarEncuesta/actualizarEncuesta.component';

@NgModule({
  declarations: [
    AppComponent,
    SecurityComponent,
    LogoutComponent,
    AdminComponent,
    BodyComponent,
    LoginComponent,
    ErrorComponent,
    EncuestaComponent,
    AdminEncuestaComponent,
    ActualizarEncuestaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    //API KEY de google maps configurado en google cloud platform
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDRlxhDKnHX5ie8Y3gJe1YOYpC4dWpa0no'
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    { provide: LocationStrategy, useClass: PathLocationStrategy }],
  bootstrap: [AppComponent],
  entryComponents: [
    ErrorComponent,
    ActualizarEncuestaComponent]
})
export class AppModule { }
