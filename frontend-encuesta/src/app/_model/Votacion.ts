import { Opcion } from "./Opcion";
export class Votacion {
    id: number;
    usuario: string;
    nombres: string;
    apellidos: string;
    edad: number;
    opcion: Opcion;
}