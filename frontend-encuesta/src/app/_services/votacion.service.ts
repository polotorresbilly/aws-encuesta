import { Injectable } from '@angular/core';
import { HOST_BACKEND } from '../_shared/constants';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Votacion } from '../_model/Votacion';

@Injectable({
  providedIn: 'root'
})
export class VotacionService {

  //urlVotacion: string = `${HOST_BACKEND}:9000/api/votacion`;
  urlVotacion: string = `https://ibogjwo4n1.execute-api.us-east-1.amazonaws.com/prod/api/votacion`;

  mensajeRegistro = new Subject<string>();

  constructor(private httpClient: HttpClient) { }

  obtenerTodosLosRegistros() {
    return this.httpClient.get<Votacion[]>(`${this.urlVotacion}/listar`);
  }

  obtenerVotacionUsuario(votacion: Votacion){
    return this.httpClient.post<Votacion>(`${this.urlVotacion}/obtenerVotacionUsuario`, votacion);
  }

  obtenerRegistros(page: number, size: number) {
    return this.httpClient.get<Votacion[]>(`${this.urlVotacion}/listarpaginado?page=${page}&size=${size}`);
  }

  guardarVotacion(votacion: Votacion) {
    return this.httpClient.post(`${this.urlVotacion}/registrar`, votacion);
  }

  eliminarVotacion(id: number) {
    return this.httpClient.delete(`${this.urlVotacion}/eliminar/${id}`);
  }
}
