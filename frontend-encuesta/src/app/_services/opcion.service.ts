import { Injectable } from '@angular/core';
import { HOST_BACKEND } from '../_shared/constants';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Opcion } from '../_model/Opcion';

@Injectable({
  providedIn: 'root'
})
export class OpcionService {

  //urlOpcion: string = `${HOST_BACKEND}:7000/api/opcion`;
  urlOpcion: string = `https://fijzsdutv7.execute-api.us-east-1.amazonaws.com/prod/api/opcion`;

  mensajeRegistro = new Subject<string>();

  constructor(private httpClient: HttpClient) { }

  obtenerTodosLosRegistros() {
    return this.httpClient.get<Opcion[]>(`${this.urlOpcion}/listar`);
  }

}
