import { Component, OnInit, ViewChild } from '@angular/core';
import { Votacion } from 'src/app/_model/Votacion';
import { MatTableDataSource, MatDialog, MatSnackBar, MatPaginator } from '@angular/material';
import { VotacionService } from 'src/app/_services/votacion.service';
import { ActualizarEncuestaComponent } from './actualizarEncuesta/actualizarEncuesta.component';

@Component({
  selector: 'adminencuesta-about',
  templateUrl: './adminencuesta.component.html',
  styleUrls: ['./adminencuesta.component.css']
})
export class AdminEncuestaComponent implements OnInit {

  dataSource:MatTableDataSource<Votacion>;
  totalElementos: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'usuario', 'nombres', 'apellidos','edad','opcion','acciones'];

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private votacionService: VotacionService
  ) {
    this.dataSource = new MatTableDataSource<Votacion>();
   }

   ngOnInit() {
    this.cargarTabla(0, 100, false);

    this.votacionService.mensajeRegistro.subscribe((dato) => {
      this.dialog.closeAll();
      this.snackBar.open(dato, null, {
        duration: 1500,
      });
      this.cargarTabla(0, 100, false);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  mostrarMas(event){
    this.cargarTabla(event.pageIndex, event.pageSize, true);
  }

  cargarTabla(pageIndex: number, pageSize: number, desdePaginador: boolean){
    this.votacionService.obtenerRegistros(pageIndex, pageSize).subscribe((datos) => {
      let registros = JSON.parse(JSON.stringify(datos)).content;
      this.dataSource = new MatTableDataSource<Votacion>(registros);
      this.totalElementos = JSON.parse(JSON.stringify(datos)).totalElements;
      if(!desdePaginador){
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  eliminarVotacion(id: number) {
    this.votacionService.eliminarVotacion(id).subscribe((data) => {
      this.votacionService.mensajeRegistro.next('Dato eliminado correctamente...');
    });
  }

  actualizarVotacion(votacion: Votacion) {
    this.dialog.open(ActualizarEncuestaComponent, {
      width: '80%',
      height: '50%',
      data: { votacion: votacion }
    });
  }

}
