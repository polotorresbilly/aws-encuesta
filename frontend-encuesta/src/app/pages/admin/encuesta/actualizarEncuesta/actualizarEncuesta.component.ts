import { Component, OnInit, Inject } from '@angular/core';
import { VotacionService } from '../../../../_services/votacion.service';
import { OpcionService } from '../../../../_services/opcion.service';
import { Opcion } from '../../../../_model/Opcion';
import { Votacion } from '../../../../_model/Votacion';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TOKEN_NAME, PARAM_USUARIO, REFRESH_TOKEN_NAME, ACCESS_TOKEN_NAME } from '../../../../_shared/constants';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-actualizar-encuesta',
  templateUrl: './actualizarencuesta.component.html',
  styleUrls: ['./actualizarencuesta.component.css']
})
export class ActualizarEncuestaComponent implements OnInit {

  votacion: Votacion;
  opciones: Opcion[] = [];
  form: FormGroup;
  encuestado : Boolean = false;

  constructor(
    private votacionService: VotacionService,
    private opcionService: OpcionService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.votacion = new Votacion();
  }

  ngOnInit() {
    if(this.data != null && this.data.votacion != null){
      this.votacion = this.data.votacion;
    }
    this.opcionService.obtenerTodosLosRegistros().subscribe((data) => {
      this.opciones = data;
    });
    this.form = this.formBuilder.group({
      names: [null, Validators.required],
      lastnames: [null, Validators.required],
      age: [null, Validators.required],
      options: [null, Validators.required],
    });
    this.form.updateValueAndValidity();
  }

  ngAfterViewChecked(){
    this.form.updateValueAndValidity();
  }

  onSubmit() {
    this.votacionService.guardarVotacion(this.votacion).subscribe((data)=>{
        this.votacionService.mensajeRegistro.next('Registrado Correctamente...');
    }, (error) => {
      this.votacionService.mensajeRegistro.next('Error al guardar el registro...');
    });
  }

}
