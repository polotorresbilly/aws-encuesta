import { Component, OnInit } from '@angular/core';
import { VotacionService } from '../../_services/votacion.service';
import { OpcionService } from '../../_services/opcion.service';
import { Opcion } from '../../_model/Opcion';
import { Votacion } from '../../_model/Votacion';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TOKEN_NAME, PARAM_USUARIO, REFRESH_TOKEN_NAME, ACCESS_TOKEN_NAME } from '../../_shared/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {

  votacion: Votacion;
  opciones: Opcion[] = [];
  form: FormGroup;
  encuestado : Boolean = false;

  constructor(
    private votacionService: VotacionService,
    private opcionService: OpcionService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.votacion = new Votacion();
    var data = JSON.parse(sessionStorage.getItem(PARAM_USUARIO));
    this.votacion.usuario = data.username;
  }

  ngOnInit() {
    console.log(this.votacion);
    this.votacionService.obtenerVotacionUsuario(this.votacion).subscribe((rs) => {
      console.log(rs);
      if (rs != undefined){
        this.encuestado = true;
      }
    });
    this.form = this.formBuilder.group({
      names: [null, Validators.required],
      lastnames: [null, Validators.required],
      age: [null, Validators.required],
      options: [null, Validators.required],
    });
    this.opcionService.obtenerTodosLosRegistros().subscribe((data) => {
      this.opciones = data;
    });
  }

  onSubmit() {
    this.votacionService.guardarVotacion(this.votacion).subscribe((data)=>{
        this.encuestado = true;
    }, (error) => {
      
    });
  }

}
