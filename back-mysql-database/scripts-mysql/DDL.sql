CREATE DATABASE IF NOT EXISTS encuestabd CHARACTER SET latin1 COLLATE latin1_swedish_ci;

USE encuestabd;

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS votacion;
DROP TABLE IF EXISTS opcion;

SET foreign_key_checks = 1;

CREATE TABLE opcion(
   	id INT(4) NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=INNODB;


CREATE TABLE votacion(
   	id INT(3) NOT NULL AUTO_INCREMENT,
	usuario varchar(50) NOT NULL,
	nombres varchar(50) NOT NULL,
	apellidos varchar(50) NOT NULL,
	edad int(5) NOT NULL,
	opcion int(3) NOT NULL,
	INDEX (opcion),
	FOREIGN KEY (opcion) REFERENCES opcion(id),
	PRIMARY KEY (`id`)
) ENGINE=INNODB;